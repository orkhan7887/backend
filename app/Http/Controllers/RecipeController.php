<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recipe;

class RecipeController extends Controller
{
    public function getRecipes(Request $request)
    {
        try {
            $recipes = Recipe::all();
            foreach ($recipes as $item){
                $item->recipe = json_decode($item->recipe);
            }
            return response()->json($recipes, 200);
        } catch (\Exception $e) {
            throw $e;
        }
    }

}
